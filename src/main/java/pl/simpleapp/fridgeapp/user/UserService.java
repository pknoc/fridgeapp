//package pl.simpleapp.fridgeapp.user;
//
//
//import org.springframework.boot.context.event.ApplicationReadyEvent;
//import org.springframework.context.event.EventListener;
//import org.springframework.security.core.userdetails.UserDetails;
//import org.springframework.security.core.userdetails.UserDetailsService;
//import org.springframework.security.core.userdetails.UsernameNotFoundException;
//import org.springframework.stereotype.Service;
//
//
//@Service
//public class UserService implements UserDetailsService {
//
//
/////*
////                                          Klasa tymczasowo wykluczona w celu analizy i naprawy problemu.
////*/
//
//
//    private UserRepository userRepository;
//
//    public UserService(UserRepository userRepository){
//        this.userRepository = userRepository;
//    }
//
//
//
//    @Override
//    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
//        return userRepository.findByUsername(username)
//                .orElseThrow(() -> new UsernameNotFoundException("Brak użytkownika."));
//    }
//
//    /*
//    Metoda dodająca nowego użytkownika do bazy danych.
//     */
//
//    public User save(User user){
//        return userRepository.save(user);
//    }
//
//    /*
//    Metoda zwracająca liste wszystkich użytkowników.
//     */
//
//    public Iterable <User> getAll() { return userRepository.findAll(); }
//
//
//    /*
//    Wstępne uzupełnienie bazy danych.
//     */
//
//    @EventListener(ApplicationReadyEvent.class)
//    public void fillDB() {
//        save(new User(2,"user1","password","aa@wp.pl","USER"));
//        save(new User(3,"user2","aaa","ahhha@wp.pl","USER"));
//        save(new User(4,"admin","password","ajjjjaa@wp.pl","ADMIN"));
//        save(new User(0,"user0","password","ajjjjaa@wp.pl","USER"));
//
//    }
//
//}
