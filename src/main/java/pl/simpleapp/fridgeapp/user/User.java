//package pl.simpleapp.fridgeapp.user;
//
//import jakarta.persistence.*;
//import org.hibernate.validator.constraints.Length;
//import org.springframework.security.core.GrantedAuthority;
//import org.springframework.security.core.userdetails.UserDetails;
//
//
//import javax.validation.constraints.Email;
//import java.util.Collection;
//
///*
//                                          Klasa tymczasowo wykluczona w celu analizy i naprawy problemu.
//*/
//
//
//
//@Entity
//@Inheritance(strategy = InheritanceType.JOINED)
//public class User implements UserDetails {
//
//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    private Integer id;
//    @Length(min = 3)
//    private String username;
//    private String password;
//    @Email
//    private String email;
//    private String role;
//
//    public User (){  }
//
//    public User(Integer id, String username, String password, String email, String role) {
//        this.id = id;
//        this.username = username;
//        this.password = password;
//        this.email = email;
//        this.role = role;
//    }
//
//    public void setId(Integer id) {
//        this.id = id;
//    }
//
//    public Integer getId() {
//        return this.id;
//    }
//
//    public void setUsername(String login) {
//        this.username = login;
//    }
//
//    @Override
//    public String getUsername() {
//        return username;
//    }
//
//    public void setPassword(String password) {
//        this.password = password;
//    }
//
//    @Override
//    public String getPassword() {
//        return password;
//    }
//
//    public void setEmail(String email) {
//        this.email = email;
//    }
//
//    public String getEmail() {
//        return this.email;
//    }
//
//    public void setRole(String role) {
//        this.role = role;
//    }
//
//    public String getRole() {
//        return role;
//    }
//
//    @Override
//    public boolean isAccountNonExpired() {
//        return true;
//    }
//
//    @Override
//    public boolean isAccountNonLocked() {
//        return true;
//    }
//
//    @Override
//    public boolean isCredentialsNonExpired() {
//        return true;
//    }
//
//    @Override
//    public boolean isEnabled() {
//        return true;
//    }
//
//
//    @Override
//    public Collection<? extends GrantedAuthority> getAuthorities() {
//        return null;
//    }
//
//
//
//
//}
