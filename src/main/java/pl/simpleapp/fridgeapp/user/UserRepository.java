//package pl.simpleapp.fridgeapp.user;
//
//import org.springframework.data.repository.CrudRepository;
//import org.springframework.stereotype.Repository;
//
//import java.util.Optional;
//
//@Repository
//public interface UserRepository extends CrudRepository<User, Integer> {
//    Optional<User> findByUsername(String username);
//    User findByEmail(String email);
//}

///*
//                                          Klasa tymczasowo wykluczona w celu analizy i naprawy problemu.
//*/
