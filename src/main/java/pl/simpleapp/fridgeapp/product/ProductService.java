package pl.simpleapp.fridgeapp.product;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import java.util.Optional;

/*
Klasa umożliwiająca operacje na rekordach w bazie danych produktów.

 */

@Service
public class ProductService {

    private ProductRepository productRepository;

    @Autowired
    public ProductService(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    /*
    Metoda umożliwiająca znalezienie produktu po jego ID.
     */

    public Optional<Product> findById(Integer id) {
        return productRepository.findById(id);
    }

    /*
    Metoda zwracająca liste wszystkich produktów z bazy danych.
     */
    public Iterable<Product> findAll() {
        return productRepository.findAll();
    }

    /*
    Metoda dodająca nowy produkt do bazy danych
     */

    public Product save(Product product) {
        return productRepository.save(product);
    }

    /*
    Metoda usuwająca rekord z bazy danych.
     */

    public void deleteById(Integer id) {
        productRepository.deleteById(id);
    }

    /*
    Wstępne wypełnienie bazy danych.
     */

    @EventListener(ApplicationReadyEvent.class)
    public void fillDB() {
        save(new Product(1,"Milk",1000));
        save(new Product(2,"Butter",400));
        save(new Product(3,"Water",3000));
        save(new Product(4,"Potato",5));
        save(new Product(5,"Eggs",20));

    }

}



