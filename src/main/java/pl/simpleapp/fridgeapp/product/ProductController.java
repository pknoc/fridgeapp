package pl.simpleapp.fridgeapp.product;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/products")
@CrossOrigin
public class ProductController {

    private ProductService productService;

    @Autowired
    public ProductController (ProductService productService){
        this.productService = productService;
    }
    @GetMapping("/all")
    public Iterable<Product> getAllProducts() {
        return productService.findAll();
    }

    @GetMapping("/info/{index}")
    public Object getById(@PathVariable Integer index) {
        return productService.findById(index);
    }

    @PostMapping("/add")
    public Product addProduct(@RequestBody Product product) {
        return productService.save(product);
    }

    @PutMapping("/update")
    public Product updateProduct(@RequestBody Product product) {
        return productService.save(product);
    }

    @DeleteMapping ("/delete/{id}")
    public void deleteProduct(@PathVariable Integer id) { productService.deleteById(id); }


}
