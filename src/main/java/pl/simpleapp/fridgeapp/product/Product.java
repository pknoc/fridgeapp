package pl.simpleapp.fridgeapp.product;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;

/*
Klasa produkt ma za zadanie tworzenie obiektu produktu. Posiada on swoje unikalne ID,
dzięki któremu możliwe jest "podpięcie" go do lodówki użytkownika, nazwe produktu oraz
ilość "pełnego" opakowania. "Pełne" opakowanie ma slużyć w przyszłości do porównywania
z obecną ilościa produktu, w przypadku zbyt małej ilości np. >30% aplikacja będzie informować
o potrzebie uzupełnienia zapasów.
 */


@Entity
public class Product {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private Integer id;
    private String name;
    private Integer fullAmount;

    /*
    Konstruktor bez argumentów.
     */

    public Product() {
    }

    /*
    Konstruktor z argumentami.
     */

    public Product(Integer id, String name, Integer fullAmount) {
        this.id = id;
        this.name = name;
        this.fullAmount = fullAmount;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getFullAmount() {
        return fullAmount;
    }

    public void setFullAmount(Integer fullAmount) {
        this.fullAmount = fullAmount;
    }

}
