package pl.simpleapp.fridgeapp.fridge;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;

/*
Klasa stworzone w celu umożliwienia przypisania produktów i ich obecnej ilości
w lodówce użytkownika.

 */


@Entity
public class Fridge {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private Integer id;
    private Integer userId;
    private Integer productId;
    private Integer productCurrentAmount;

    /*
    Konstruktor z argumentami.
     */

    public Fridge(Integer id, Integer userId, Integer productId, Integer productCurrentAmount) {
        this.id = id;
        this.userId = userId;
        this.productId = productId;
        this.productCurrentAmount = productCurrentAmount;
    }
    /*
    Konstruktor bez argumentów.
     */

    public Fridge() {

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public Integer getProductCurrentAmount() {
        return productCurrentAmount;
    }

    public void setProductCurrentAmount(Integer productCurrentAmount) {
        this.productCurrentAmount = productCurrentAmount;
    }
}
