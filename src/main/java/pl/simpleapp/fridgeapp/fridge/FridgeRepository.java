package pl.simpleapp.fridgeapp.fridge;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


@Repository
public interface FridgeRepository extends JpaRepository<Fridge, Integer> {




    @Query(value = "select * from fridge where user_Id = :userId",
            nativeQuery = true)
    Iterable<Fridge> findAllByUserId(@Param("userId") Integer userId);

}
