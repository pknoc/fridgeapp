package pl.simpleapp.fridgeapp.fridge;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

/*
Service that provides from Fridge class.
With this class we can operate around the data of Fridges, more like Fridges records.
Basic functions of app:
delete- removing record,
findById- find record by its Id
findAll- all records from database
findByUserId- this function is the most important, returning a list of a
products attached to a user. Creates a Fridge-list of a user.


 */


import java.util.Optional;

@Service
public class FridgeService {

    private FridgeRepository fridgeRepository;

    @Autowired
    public FridgeService (FridgeRepository fridgeRepository){
        this.fridgeRepository = fridgeRepository;
    }

    /*
    Metoda zwracająca rekord z bazy danych dzięki jego ID.
     */

    public Optional<Fridge> findById(Integer id) {
        return fridgeRepository.findById(id);
    }

    public Iterable<Fridge> findAll() {
        return fridgeRepository.findAll();
    }

    /*
    Metoda dodająca nowy rekord do bazy danych.
     */

    public Fridge save(Fridge fridge) {
        return fridgeRepository.save(fridge);
    }

    /*
    Metoda usuwająca rekord z bazy danych, przy użyciu jego ID.
     */

    public void deleteById(Integer id) {
        fridgeRepository.deleteById(id);
    }

    public Iterable<Fridge> findByUserId(Integer userId) {
        return fridgeRepository.findAllByUserId(userId);
    }


/*
    Wstępne wypełnienie bazy danych.
 */

    @EventListener(ApplicationReadyEvent.class)
    public void fillDB() {
        save(new Fridge(1, 1, 0,500));
        save(new Fridge(1, 1, 1,300));
        save(new Fridge(2, 1, 3,5));
        save(new Fridge(3, 1, 4,20));
        save(new Fridge(4, 2, 1,200));
        save(new Fridge(5, 2, 2,2000));
        save(new Fridge(6, 2, 3,3));

    }
}
