package pl.simpleapp.fridgeapp.fridge;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/fridge")
@CrossOrigin
public class FridgeController {

    private FridgeService fridgeService;

    @Autowired
    public FridgeController(FridgeService fridgeService) {
        this.fridgeService = fridgeService;
    }

    /*
    Metoda zwracająca wszystkie rekordy dla "lodówek".
     */

    @GetMapping("/all")
    public Iterable<Fridge> getAllFridgesData() {
        return fridgeService.findAll();
    }

    /*
    Metoda dodająca nowy rekord do bazy danych.
     */

    @PostMapping("/add")
    public Fridge addFridgeRecord(@RequestBody Fridge fridge) {
        return fridgeService.save(fridge);
    }
    /*
    Metoda edytująca istniejący już rekord. Stworzona do edycji obecnej ilości produktu w lodówce.
     */

    @PutMapping("/put")
    public Fridge editAmountInFridge (@RequestBody Fridge fridge) {
        return fridgeService.save(fridge);
    }

    /*
    Metoda do usuwania rekordu z bazy danych- produktu z lodówki użytkownika.
     */

    @DeleteMapping("/delete/{index}")
    public void deleteFridge(@PathVariable Integer index) {
        fridgeService.deleteById(index);
    }

    /*
    Metoda zwracająca liste- zawartość, lodówki użytkownika.
     */

    @GetMapping("/{index}/list")
    public Iterable<Fridge> getFridgeList(@PathVariable Integer index) {

        return fridgeService.findByUserId(index);
    }
}
