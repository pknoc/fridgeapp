package pl.simpleapp.fridgeapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.boot.autoconfigure.security.servlet.UserDetailsServiceAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;


@EnableConfigurationProperties(RsaKeyProperties.class)
@SpringBootApplication(exclude = {SecurityAutoConfiguration.class})
public class FridgeAppApplication {

    public static void main(String[] args) {
        SpringApplication.run(FridgeAppApplication.class, args);
    }

}
